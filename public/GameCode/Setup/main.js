const SetupGame = () => {

    //Load the images
    SetupImages();

    //For the login and sign up windows
    SetUpLogin();
    SetUpSignup();


    
    //Loads the restaurant default layout
    SetupRestaurant();

    //Load the game interface
    setupUI();

    //Loads the stages
    CookSetUp();
    
};

